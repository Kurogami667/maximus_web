Rails.application.routes.draw do
  # Letter Opener views
  if Rails.env.development? || Rails.env.test?
    mount LetterOpenerWeb::Engine, at: "/letter_opener"
  end

  get 'home/index'
  root to: 'home#index'
end
