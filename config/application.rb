require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module MaximusWeb
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.0

    config.generators do |g|
      g.assets false
      g.helper false
      g.stylesheets false
      g.template_engine :slim
    end

    config.i18n.load_path += Dir[Rails.root.join('config/locales/**/*.{rb,yml}')]
    config.i18n.available_locales = %i[es en]
    config.i18n.default_locale = :es

    config.active_record.default_timezone = :local
    config.autoload_paths << Rails.root.join('lib')

    config.assets.initialize_on_precompile = false
    # config.assets.precompile << /\.(?:svg|eot|woff|ttf)$/
    config.assets.paths << Rails.root.join('app/assets/fonts')

    config.time_zone = 'Mexico City'

  end
end
