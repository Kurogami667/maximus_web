source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '3.0.1'

gem 'active_storage_validations'
gem 'audited'
gem 'aws-sdk-s3'
gem 'bootsnap', '>= 1.4.2', require: false
gem 'cocoon'
gem 'coffee-rails'
gem 'enum_help'
gem 'friendly_id'
gem 'image_processing'
gem 'jbuilder'
gem 'name_of_person'
gem 'noticed'
gem 'pagy'
gem 'paranoia'
gem 'pg'
gem 'puma'
gem 'rails', '6.1.4'
gem 'redis'
gem 'sass-rails'
gem 'simple_form'
gem 'slim-rails'
gem 'turbolinks', '~> 5'
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
gem 'webpacker'

group :development, :test do
  gem 'annotate'
  gem 'brakeman'
  gem 'bundler-audit', github: 'rubysec/bundler-audit'
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'database_cleaner'
  gem 'factory_bot_rails'
  gem 'faker'
  gem 'letter_opener_web'
  gem 'rails-controller-testing'
  gem 'rspec-rails'
end

group :development do
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'listen'
  gem 'rack-mini-profiler'
  gem 'rubocop'
  gem 'rubocop-gitlab-security'
  gem 'rubocop-rails'
  gem 'rubocop-rspec'
  gem 'spring'
  gem 'web-console'
end

group :test do
  gem 'capybara'
  gem 'fuubar'
  gem 'selenium-webdriver'
  gem 'shoulda-matchers'
  gem 'simplecov', '~> 0.21.2', require: false
  gem 'webdrivers'
end
